Fettered game system
=============
This is a Foundry Virtual Tabletop game system adaptation of the Fettered
tabletop role-playing game. Fettered is a homebrew derivative of the New World
of Darkness game system and setting.

Installation Instructions
-------------
Start on the Configuration and Setup page of your Foundry Virtual Tabletop installation.

Click the Game Systems tab.

Click the Install System button.

Enter the following Manifest URL:
https://bitbucket.org/weddendorf/fettered/raw/HEAD/system.json

Click Install.
